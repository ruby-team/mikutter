# frozen_string_literal: true

module Plugin::ActivityGtk
  class ActivityView < Gtk::CompatListView
    include ::Gtk::TreeViewPrettyScroll

    ICON = 0
    KIND = 1
    TITLE = 2
    DATE = 3
    MODEL = 4
    URI = 5

    def initialize(plugin)
      type_strict plugin => Plugin
      @plugin = plugin
      super()
    end

    def column_schemer
      [
        { kind: :pixbuf, type: GdkPixbuf::Pixbuf, label: 'icon' }, # ICON
        { kind: :text, type: String, label: _('種類') },      # KIND
        { kind: :text, type: String, label: _('説明') },      # TITLE
        { kind: :text, type: String, label: _('時刻') },      # DATE
        { type: Plugin::Activity::Activity },                         # Activity Model
        { type: String }                                              # URI
      ].freeze
    end

    def method_missing(...)
      @plugin.__send__(...)
    end

    def respond_to_missing?(...)
      @plugin.respond_to?(...)
    end
  end
end
