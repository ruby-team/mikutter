# -*- coding: utf-8 -*-

require 'mui/gtk_form_dsl'
require 'mui/gtk_form_dsl_multi_select'
require 'mui/gtk_form_dsl_select'

module Plugin::ExtractGtk
end

class Plugin::ExtractGtk::OptionWidget < Gtk::Grid
  include Gtk::FormDSL

  def create_inner_setting
    self.class.new(@plugin, @extract)
  end

  def initialize(plugin, extract)
    @plugin = plugin
    @extract = extract

    super()
    self.row_spacing = self.column_spacing = self.margin = 12
  end

  def [](key)
    case key
    when :icon, :sound
      @extract[key].to_s
    else
      @extract[key]
    end
  end

  def []=(key, value)
    @extract[key] = case key
                    when :icon, :sound
                      value.empty? ? nil : value
    else
                      value
                    end
    @extract.notify_update
  end

  def method_missing(method, *, &)
    @plugin.send(method, *, &)
  end

  def respond_to_missing?(method, include_private=false)
    @plugin.respond_to?(method, include_private)
  end
end
