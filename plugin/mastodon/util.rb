module Plugin::Mastodon
  class Util
    VISIBILITY2SELECT = {
      'public' => :'1public',
      'unlisted' => :'2unlisted',
      'private' => :'3private',
      'direct' => :'4direct'
    }.freeze
    SELECT2VISIBILITY = VISIBILITY2SELECT.to_h { |k, v| [v, k] }
    class << self
      def deep_dup(obj)
        Marshal.load(Marshal.dump(obj))
      end

      def ppf(obj)
        pp obj
        $stdout.flush
      end

      def visibility2select(visibility)
        VISIBILITY2SELECT[visibility]
      end

      def select2visibility(select)
        SELECT2VISIBILITY[select]
      end
    end
  end
end
