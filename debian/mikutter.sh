#!/bin/sh

export DISABLE_BUNDLER_SETUP=1
exec /usr/bin/ruby /usr/share/mikutter/mikutter.rb "$@"
