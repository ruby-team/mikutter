# frozen_string_literal: true

desc 'mikutterで普段やるlint'
task lint: ['rubocop:unblacklist', 'rubocop']

namespace :lint do
  desc 'lintとautocorrect(-A)'
  task fix: ['rubocop:unblacklist', 'rubocop:autocorrect_all']
end
