# -*- coding: utf-8 -*-

require 'gtk3'

module Gtk::InnerTLDarkMatterPurification
  attr_accessor :collect_counter

  def initialize(*)
    @collect_counter = 256
    super(*)
  end
end

module Gtk::TimelineDarkMatterPurification
  def initialize(*)
    super(*)
    refresh_timer
  end

  # InnerTLをすげ替える。
  def refresh
    notice 'timeline refresh'
    scroll = @tl.vadjustment.value
    oldtl = @tl
    @tl = Gtk::TimeLine::InnerTL.new(oldtl)
    remove(@shell)
    @shell = init_tl
    @tl.vadjustment.value = scroll
    pack_start(@shell.show_all)
    @exposing_miraclepainter = []
    oldtl.destroy unless oldtl.destroyed?
  end

  # ある条件を満たしたらInnerTLを捨てて、全く同じ内容の新しいInnerTLにすげ替えるためのイベントを定義する。
  def refresh_timer
    Delayer.new(delay: 60) do
      unless @tl.destroyed?
        window_active = Plugin.filtering(:get_windows, []).first.any?(&:has_toplevel_focus?)
        @tl.collect_counter -= 1 unless window_active
        refresh if !((Gtk::TimeLine::InnerTL.current_tl == @tl) && window_active && (Plugin.filtering(:get_idle_time, nil).first < 3600)) && (@tl.collect_counter <= (window_active ? -HYDE : 0))
        refresh_timer end
    end
  end

  def tl_model_remove(iter)
    iter[Gtk::TimeLine::InnerTL::MIRACLE_PAINTER].destroy
    @tl.model.remove(iter)
    @tl.collect_counter -= 1
  end
end
